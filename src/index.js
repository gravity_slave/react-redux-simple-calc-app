import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import { Provider } from 'react-redux';
import { createStore } from 'redux';
import rootReducer from  './reducers/index';
import {incrementNumber} from "./actions/index";

const store =  createStore(rootReducer);
store.subscribe(() => console.log('store', store.getState()));

store.dispatch(incrementNumber(store.getState()));
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
 , document.getElementById('root'));
registerServiceWorker();
